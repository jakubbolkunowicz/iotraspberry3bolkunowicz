// Copyright (c) Microsoft. All rights reserved.

namespace LibOneWire {
  public interface OneWireDevice {
    DS2482_100 DS2482_100 { get; set; }
    byte[] OneWireAddress { get; set; }
    void Initialize(byte sts);
  }
}
