// Copyright (c) Microsoft. All rights reserved.
using System;
using System.Threading.Tasks;
using Windows.Devices.Enumeration;
using Windows.Devices.I2c;

namespace LibOneWire {
  class I2cDeviceLocator {
    const string I2C_CONTROLLER_NAME = "I2C1";                        // For Raspberry Pi 2: use I2C1, For Minnowboard Max: use I2C5

    public async Task<I2cDevice> GetI2cDevice(byte address) {
      string aqs = I2cDevice.GetDeviceSelector(I2C_CONTROLLER_NAME);  // Get a selector string that will return all I2C controllers on the system
      var dis = await DeviceInformation.FindAllAsync(aqs);            // Find the I2C bus controller device with our selector string
      if (dis.Count == 0) {
        throw new InvalidOperationException("I2C:No controllers were found on the system!");
      }
      var OneWireSettings = new I2cConnectionSettings(address);
      OneWireSettings.BusSpeed = I2cBusSpeed.FastMode;
      OneWireSettings.SharingMode = I2cSharingMode.Shared;

      return await I2cDevice.FromIdAsync(dis[0].Id, OneWireSettings); // Create an I2cDevice with our selected bus controller and I2C settings

    } // End of GetI2cDevice

    public async Task<I2cDevice> GetI2cDevice1(byte address) {
      string aqs     = I2cDevice.GetDeviceSelector(I2C_CONTROLLER_NAME);                          // Get a selector string that will return all I2C controllers on the system
      var I2CDevices = await DeviceInformation.FindAllAsync(aqs).AsTask().ConfigureAwait(false);  // Find the I2C bus controller device with our selector string

      if (I2CDevices.Count == 0) {
        throw new InvalidOperationException("No I2C controllers were found on the system");
      }
      var I2Csettings         = new I2cConnectionSettings(0x45);
      I2Csettings.BusSpeed    = I2cBusSpeed.FastMode;
      I2Csettings.SharingMode = I2cSharingMode.Shared;

      var I2CDev1 = await I2cDevice.FromIdAsync(I2CDevices[0].Id, I2Csettings).AsTask().ConfigureAwait(false);

      I2Csettings             = new I2cConnectionSettings(address);
      I2Csettings.BusSpeed    = I2cBusSpeed.FastMode;
      I2Csettings.SharingMode = I2cSharingMode.Shared;

      var I2CDev2 = await I2cDevice.FromIdAsync(I2CDevices[0].Id, I2Csettings).AsTask().ConfigureAwait(false);

      return I2CDev2;        // Create an I2cDevice with our selected bus controller and I2C settings

    } // End of GetI2cDevice1
  }
}

