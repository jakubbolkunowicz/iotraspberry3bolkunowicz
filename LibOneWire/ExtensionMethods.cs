// Copyright (c) Microsoft. All rights reserved.
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace LibOneWire {
  public static class ExtensionMethods {
    public static bool GetBit(this byte b, int bitNumber) {

      return (b & (1 << bitNumber)) != 0;

    } // End of GetBit

    public static IEnumerable<T> GetDevices<T>(this IEnumerable<OneWireDevice> devices) where T : OneWireDevice {
      var result = new List<T>();
      foreach (var item in devices.Where(dev => dev.GetType().Equals(typeof(T)))) {
        result.Add((T)item);
      }
      return result;

    } // End of GetDevices

    //public static T Deserialize<T>(this string toDeserialize) {
    //  XmlSerializer xmlSerializer = new XmlSerializer(typeof(T));
    //  StringReader textReader = new StringReader(toDeserialize);
    //  return (T)xmlSerializer.Deserialize(textReader);
    //}

    //public static string Serialize<T>(this T toSerialize) {
    //  XmlSerializer xmlSerializer = new XmlSerializer(typeof(T));
    //  StringWriter textWriter = new StringWriter();
    //  xmlSerializer.Serialize(textWriter, toSerialize);
    //  return textWriter.ToString();
    //}

    public static string XmlSerializeToString(this object objectInstance) {
      var serializer = new XmlSerializer(objectInstance.GetType());
      var sb = new StringBuilder();

      using (TextWriter writer = new StringWriter(sb)) {
        serializer.Serialize(writer, objectInstance);
      }
      return sb.ToString();

    } // End of XmlSerializeToString

    public static object XmlDeserializeFromString(this string objectData, Type type) {
      var serializer = new XmlSerializer(type);
      object result;

      using (TextReader reader = new StringReader(objectData)) {
        result = serializer.Deserialize(reader);
      }
      return result;

    } // End of XmlDeserializeFromString

    public static T XmlDeserializeFromString<T>(this string objectData) {

      return (T)XmlDeserializeFromString(objectData, typeof(T));

    } // End of XmlDeserializeFromString
  }
}
