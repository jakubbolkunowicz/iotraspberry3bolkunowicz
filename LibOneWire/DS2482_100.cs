// Copyright (c) Microsoft. All rights reserved.
using System;
using System.Diagnostics;
using System.Linq;
using Windows.Devices.I2c;

namespace LibOneWire {
  //
  // This implementation is based on Maxim sample implementation and has parts from converted C code
  // source: http://www.maximintegrated.com/en/app-notes/index.mvp/id/187
  //
  public class DS2482_100 : IDisposable {
    readonly I2cDevice _i2cDevice;

    // Enable leaky abstraction
    // public I2cDevice I2CDevice { get { return _i2cDevice; } }
    public DS2482_100(I2cDevice i2cDevice) {
      _i2cDevice = i2cDevice;
      ROM_NO = new byte[8];
    } // End of DS2482_100

    // global search state
    public byte[] ROM_NO { get; set; }
    public string Description;
    public byte   DevStatus;
    public bool   ReadOK = false;
    public byte   crc8;

    int LastDiscrepancy;
    int LastFamilyDiscrepancy;
    bool LastDeviceFlag;

    // Find the 'first' devices on the 1-Wire bus
    // <returns>true : device found, ROM number in ROM_NO buffer, false : no device present</returns>
    public bool OneWireFirst() {
      // reset the search state
      LastDiscrepancy       = 0;
      LastDeviceFlag        = false;
      LastFamilyDiscrepancy = 0;
      Description           = "Dev 1-Wire";
      DevStatus             = 0xFF;

      return OneWireSearch();

    } // End of OneWireFirst

    // Find the 'next' devices on the 1-Wire bus
    // <returns>true : device found, ROM number in ROM_NO buffer, false : device not found, end of search</returns>
    public bool OneWireNext() {
      // leave the search state alone
      return OneWireSearch();

    } // End of OneWireNext

    // Perform the 1-Wire Search Algorithm on the 1-Wire bus using the existing search state.
    // <returns>true : device found, ROM number in ROM_NO buffer, false : device not found, end of search</returns>
    public bool OneWireSearch() {
      int id_bit_number;
      int last_zero, rom_byte_number;
      bool id_bit, cmp_id_bit, search_direction, search_result;
      byte rom_byte_mask;

      // initialize for search
      id_bit_number   = 1;
      last_zero       = 0;
      rom_byte_number = 0;
      rom_byte_mask   = 1;
      search_result   = false;
      crc8            = 0;

      if (!LastDeviceFlag)  {                         // if the last call was not the last one
        if (!OneWireReset()) {                        // reset the search
          LastDiscrepancy       = 0;
          LastDeviceFlag        = false;
          LastFamilyDiscrepancy = 0;
          return false;
        }
        OneWireWriteByte(FunctionCommand.ROM_SEARCH); // issue the search command 
        do {                                          // loop to do the search
          id_bit     = OneWireReadBit();              // read a bit and its complement
          cmp_id_bit = OneWireReadBit();
          
          if (id_bit && cmp_id_bit)                   // check for no devices on 1-wire
            break;
          else {
            if (id_bit != cmp_id_bit)                 // all devices coupled have 0 or 1
              search_direction = id_bit;              // bit write value for search
            else {
              if (id_bit_number < LastDiscrepancy)    // if this discrepancy if before the Last Discrepancy on a previous next then pick the same as last time
                search_direction = ((ROM_NO[rom_byte_number] & rom_byte_mask) > 0);
              else
                search_direction = (id_bit_number == LastDiscrepancy);  // if equal to last pick 1, if not then pick 0
              if (!search_direction) {                                  // if 0 was picked then record its position in LastZero
                last_zero = id_bit_number;
                if (last_zero < 9)                                      // check for Last discrepancy in family
                  LastFamilyDiscrepancy = last_zero;
              }
            }
            if (search_direction)                                       // set or clear the bit in the ROM byte rom_byte_number with mask rom_byte_mask
              ROM_NO[rom_byte_number] |= rom_byte_mask;
            else {
              var result = (byte)~rom_byte_mask;
              ROM_NO[rom_byte_number] &= result;
            }
            OneWireWriteBit(search_direction);                          // serial number search direction write bit
            id_bit_number++;                                            // increment the byte counter id_bit_number and shift the mask rom_byte_mask
            rom_byte_mask <<= 1;
            if (rom_byte_mask == 0) {                                   // if the mask is 0 then go to new SerialNum byte rom_byte_number and reset mask
              do_tcrc8(ROM_NO[rom_byte_number]);                        // accumulate the CRC
              rom_byte_number++;
              rom_byte_mask = 1;
            }
          }
        }
        while (rom_byte_number < 8);                                    // loop until through all ROM bytes 0-7
        if (!((id_bit_number < 65) || (crc8 != 0))) {                   // if the search was successful then
          LastDiscrepancy = last_zero;                                  // search successful so set LastDiscrepancy,LastDeviceFlag,search_result
          if (LastDiscrepancy == 0)                                     // check for last device
            LastDeviceFlag = true;
          search_result = true;
        }
      }
      if (!search_result || ROM_NO[0] == 0) {                           // if no device found then reset counters so next 'search' will be like a first
        LastDiscrepancy = 0;
        LastDeviceFlag = false;
        LastFamilyDiscrepancy = 0;
        search_result = false;
      }
      DevStatus = 0x0F;
      var bit1 = OneWireReadBit();
      var bit2 = OneWireReadBit();
      if ((bit1 && bit2) == true)
        DevStatus = 0x00;
      if ((bit1 && bit2) == false)
        DevStatus = 0x01;

      return search_result;

    } // End of OneWireSearch

    // Verify the device with the ROM number in ROM_NO buffer is present.
    // <returns>true : device verified present, false : device not present</returns>
    public bool OneWireVerify() {
      byte[] rom_backup = new byte[8];
      int i, ld_backup, lfd_backup;
      bool   ldf_backup, rslt;
      
      for (i = 0; i < 8; i++)                         // keep a backup copy of the current state
        rom_backup[i] = ROM_NO[i];
      ld_backup       = LastDiscrepancy;
      ldf_backup      = LastDeviceFlag;
      lfd_backup      = LastFamilyDiscrepancy;
      LastDiscrepancy = 64;                           // set search to find the same device
      LastDeviceFlag  = false;

      if (OneWireSearch()) {
        rslt = true;                                  // check if same device found
        for (i = 0; i < 8; i++) {
          if (rom_backup[i] != ROM_NO[i]) {
            rslt = false;
            break;
          }
        }
      }
      else
        rslt = false;
      for (i = 0; i < 8; i++)                         // restore the search state 
        ROM_NO[i] = rom_backup[i];

      LastDiscrepancy       = ld_backup;
      LastDeviceFlag        = ldf_backup;
      LastFamilyDiscrepancy = lfd_backup;
      
      return rslt;                                    // return the result of the verify

    } // End of OneWireVerify

    // Setup the search to find the device type 'family_code' on the next call to OWNext() if it is present.
    // <param name="family_code"></param>
    public void OneWireTargetSetup(byte family_code)  {
      int i;

      ROM_NO[0] = family_code;                        // set the search state to find SearchFamily type devices
      for (i = 1; i < 8; i++)
        ROM_NO[i] = 0;
      LastDiscrepancy       = 64;
      LastFamilyDiscrepancy = 0;
      LastDeviceFlag        = false;

    } // End of OneWireTargetSetup

    // Setup the search to skip the current device type on the next call to OWNext().
    public void OneWireFamilySkipSetup() {
      
      LastDiscrepancy = LastFamilyDiscrepancy;        // set the Last discrepancy to last family discrepancy
      LastFamilyDiscrepancy = 0;

      if (LastDiscrepancy == 0)                       // check for end of list
        LastDeviceFlag = true;

    } // End of OneWireFamilySkipSetup

    //--------------------------------------------------------------------------
    // 1-Wire Functions to be implemented for a particular platform
    //--------------------------------------------------------------------------

    // Reset the 1-Wire bus and return the presence of any device
    // <returns>true : device present, false : no device present</returns>
    public bool OneWireReset() {
      // platform specific TMEX API TEST BUILD
      //return (TMTouchReset(session_handle) == 1);

      _i2cDevice.Write(new byte[] { FunctionCommand.ONEWIRE_RESET });

      var status = ReadStatus();

      if (status.GetBit(StatusBit.ShortDetected)) {
        throw new InvalidOperationException("1-WIRE short detected!");
      }
      return status.GetBit(StatusBit.PresencePulseDetected);

    } // End of OneWireReset

    // Read status byte from DS2482_100
    // <param name="setReadPointerToStatus">Set to true if read pointer should be moved to status register before reading status</param>
    public byte ReadStatus(bool setReadPointerToStatus = false) {
      var statusBuffer = new byte[1];
      if (setReadPointerToStatus) {
        _i2cDevice.WriteRead(new byte[] { FunctionCommand.SET_READ_POINTER, RegisterSelection.I2C_STATUS }, statusBuffer);
      }
      else {
        _i2cDevice.Read(statusBuffer);
      }
      if (statusBuffer.Length < 1) {
        throw new InvalidOperationException("1-WIRE: Read status failed!");
      }
      var stopWatch = new Stopwatch();
      do {
        if (stopWatch.ElapsedMilliseconds > 1) {
          throw new InvalidOperationException("1-WIRE: Bus busy for too long!");
        }
        _i2cDevice.Read(statusBuffer);
      } while (statusBuffer[0].GetBit(StatusBit.OneWireBusy));
      return statusBuffer[0];

    } // End of ReadStatus

    void WaitForOneWireReady() {
      var status = new byte[1];
      var stopWatch = new Stopwatch();

      do {
        if (stopWatch.ElapsedMilliseconds > 5000) {
          throw new InvalidOperationException("1-WIRE: Bus busy for too long!");
        }
        _i2cDevice.WriteRead(new byte[] { FunctionCommand.SET_READ_POINTER, RegisterSelection.I2C_STATUS }, status);
      } while (status[0].GetBit(StatusBit.OneWireBusy));

    } // End of WaitForOneWireReady

    // </summary>
    // <param name="byte_value">byte to send</param>
    public void OneWireWriteByte(byte byte_value) {
      // platform specific TMEX API TEST BUILD
      //TMTouchByte(session_handle, byte_value);

      _i2cDevice.Write(new byte[] { FunctionCommand.ONEWIRE_WRITE_BYTE, byte_value });

      ReadStatus();

    } // End of OneWireWriteByte

    // Send 1 bit of data to teh 1-Wire bus 
    // <param name="bit_value"></param>
    public void OneWireWriteBit(bool bit_value) {
      // platform specific TMEX API TEST BUILD
      //TMTouchBit(session_handle, (short)bit_value);

      var byte_value = new byte();

      if (bit_value) {
        byte_value |= 1 << 7;
      }
      _i2cDevice.Write(new byte[] { FunctionCommand.ONEWIRE_SINGLE_BIT, byte_value });
      ReadStatus();

    } // End of OneWireWriteBit

    // Read 1 bit of data from the 1-Wire bus 
    // <returns>true : bit read is 1, false : bit read is 0</returns>
    public bool OneWireReadBit() {
      // platform specific TMEX API TEST BUILD
      //return (byte)TMTouchBit(session_handle, 0x01);

      var byte_value = new byte();

      byte_value |= 1 << 7;
      _i2cDevice.Write(new[] { FunctionCommand.ONEWIRE_SINGLE_BIT, byte_value });

      var status = ReadStatus();

      return status.GetBit(StatusBit.SingleBitResult);

    } // End of OneWireReadBit

    // Read 1 bit of data from the 1-Wire bus 
    // <returns>true : bit read is 1, false : bit read is 0</returns>
    public byte OneWireReadByte() {
      var buffer = new byte[1];

      _i2cDevice.Write(new byte[] { DS2482_100.FunctionCommand.ONEWIRE_READ_BYTE });
      ReadStatus();
      _i2cDevice.WriteRead(new byte[] { DS2482_100.FunctionCommand.SET_READ_POINTER, DS2482_100.RegisterSelection.I2C_READ_DATA }, buffer);

      return buffer[0];

    } // End of OneWireReadByte

    public void EnableStrongPullup() {
      var configuration = new byte();
      configuration |= 1 << 2;
      configuration |= 1 << 7;
      configuration |= 1 << 5;
      configuration |= 1 << 4;

      _i2cDevice.Write(new byte[] { DS2482_100.FunctionCommand.WRITE_CONFIGURATION, configuration });

    } // End of EnableStrongPullup

    // CRCR8 256 elements table 
    static byte[] CRC_table = (new[] {
      0x00, 0x5e, 0xbc, 0xe2, 0x61, 0x3f, 0xdd, 0x83, 0xc2, 0x9c, 0x7e, 0x20, 0xa3, 0xfd, 0x1f, 0x41,
      0x9d, 0xc3, 0x21, 0x7f, 0xfc, 0xa2, 0x40, 0x1e, 0x5f, 0x01, 0xe3, 0xbd, 0x3e, 0x60, 0x82, 0xdc,
      0x23, 0x7d, 0x9f, 0xc1, 0x42, 0x1c, 0xfe, 0xa0, 0xe1, 0xbf, 0x5d, 0x03, 0x80, 0xde, 0x3c, 0x62,
      0xbe, 0xe0, 0x02, 0x5c, 0xdf, 0x81, 0x63, 0x3d, 0x7c, 0x22, 0xc0, 0x9e, 0x1d, 0x43, 0xa1, 0xff,
      0x46, 0x18, 0xfa, 0xa4, 0x27, 0x79, 0x9b, 0xc5, 0x84, 0xda, 0x38, 0x66, 0xe5, 0xbb, 0x59, 0x07,
      0xdb, 0x85, 0x67, 0x39, 0xba, 0xe4, 0x06, 0x58, 0x19, 0x47, 0xa5, 0xfb, 0x78, 0x26, 0xc4, 0x9a,
      0x65, 0x3b, 0xd9, 0x87, 0x04, 0x5a, 0xb8, 0xe6, 0xa7, 0xf9, 0x1b, 0x45, 0xc6, 0x98, 0x7a, 0x24,
      0xf8, 0xa6, 0x44, 0x1a, 0x99, 0xc7, 0x25, 0x7b, 0x3a, 0x64, 0x86, 0xd8, 0x5b, 0x05, 0xe7, 0xb9,
      0x8c, 0xd2, 0x30, 0x6e, 0xed, 0xb3, 0x51, 0x0f, 0x4e, 0x10, 0xf2, 0xac, 0x2f, 0x71, 0x93, 0xcd,
      0x11, 0x4f, 0xad, 0xf3, 0x70, 0x2e, 0xcc, 0x92, 0xd3, 0x8d, 0x6f, 0x31, 0xb2, 0xec, 0x0e, 0x50,
      0xaf, 0xf1, 0x13, 0x4d, 0xce, 0x90, 0x72, 0x2c, 0x6d, 0x33, 0xd1, 0x8f, 0x0c, 0x52, 0xb0, 0xee,
      0x32, 0x6c, 0x8e, 0xd0, 0x53, 0x0d, 0xef, 0xb1, 0xf0, 0xae, 0x4c, 0x12, 0x91, 0xcf, 0x2d, 0x73,
      0xca, 0x94, 0x76, 0x28, 0xab, 0xf5, 0x17, 0x49, 0x08, 0x56, 0xb4, 0xea, 0x69, 0x37, 0xd5, 0x8b,
      0x57, 0x09, 0xeb, 0xb5, 0x36, 0x68, 0x8a, 0xd4, 0x95, 0xcb, 0x29, 0x77, 0xf4, 0xaa, 0x48, 0x16,
      0xe9, 0xb7, 0x55, 0x0b, 0x88, 0xd6, 0x34, 0x6a, 0x2b, 0x75, 0x97, 0xc9, 0x4a, 0x14, 0xf6, 0xa8,
      0x74, 0x2a, 0xc8, 0x96, 0x15, 0x4b, 0xa9, 0xf7, 0xb6, 0xe8, 0x0a, 0x54, 0xd7, 0x89, 0x6b, 0x35
    }).Select(x => (byte)x).ToArray();

    public byte dallas_tcrc8(byte[] data) {
      crc8 = 0;

      for (int i = 0; i < data.Length - 1; ++i) {
        crc8 = CRC_table[crc8 ^ data[i]];
      }
      return crc8;
    } // End of dallas_tcrc8

    public byte dallas_fcrc8(byte[] data) {
      crc8 = 0;

      for (int i = 0; i < data.Length - 1; ++i) {
        byte inbyte = data[i];
        for (byte j = 0; j < 8; ++j) {
          byte mix = (byte)((byte)(crc8 ^ inbyte) & 0x01);
          crc8 >>= 1;
          if (mix != 0) crc8 ^= 0x8C;
          inbyte >>= 1;
        }
      }
      return crc8;

    } // End of dallas_fcrc8


    //--------------------------------------------------------------------------
    // Calculate the CRC8 using CRC8 table of the byte value provided with the current 
    // global 'crc8' value. 
    // Returns current global crc8 value
    //
    public byte do_tcrc8(byte value) {
      // See Application Note 27 TEST BUILD
      crc8 = CRC_table[crc8 ^ value];

      return crc8;

    } // End of do_tcrc8

    //--------------------------------------------------------------------------
    // Calculate the CRC8 not using CRC8 table of the byte value provided with the current 
    // global 'crc8' value. 
    // Returns current global crc8 value
    //
    public byte do_fcrc8(byte value) {
      for (byte j = 0; j < 8; ++j) {
        byte mix = (byte)((byte)(crc8 ^ value) & 0x01);
        crc8 >>= 1;
        if (mix != 0) crc8 ^= 0x8C;
        value >>= 1;
      }
      return crc8;

    } // End of do_fcrc8

    public void Dispose() {

      _i2cDevice.Dispose();

    } // End of Dispose

    public class FunctionCommand  {
      public const byte DEVICE_RESET        = 0xF0;
      public const byte SET_READ_POINTER    = 0xE1;
      public const byte WRITE_CONFIGURATION = 0xD2;
      public const byte ONEWIRE_RESET       = 0xB4;
      public const byte ONEWIRE_SINGLE_BIT  = 0x87;
      public const byte ONEWIRE_WRITE_BYTE  = 0xA5;
      public const byte ONEWIRE_READ_BYTE   = 0x96;
      public const byte ONEWIRE_TRIPLET     = 0x78;

      // ROM Commands
      public const byte ROM_SEARCH          = 0xF0;
      public const byte ROM_READ            = 0x33;
      public const byte ROM_MATCH           = 0x55;
      public const byte ROM_SKIP            = 0xCC;

      // DS 1820 Control Function Commands
      public const byte CONVERT_T           = 0x44;
      public const byte READ_SCRATCHPAD     = 0xBE;

      // DS2408 Register Address Map
      public const byte PIO_LOGIC_STATE     = 0x88;
      public const byte PIO_OUTPUT_STATE    = 0x89;
      public const byte PIO_ACTIV_READ      = 0x8A;
      public const byte PIO_CTRL_STATE      = 0x8D;
      public const byte DUMMY_BYTE          = 0xFF;
      public const byte ADDR_HIGH           = 0x00;
      public const byte ADDR_STATE          = 0x88;

      // DS2406 DS2413 DS2408 Control Function Commands
      public const byte PIO_READ_REGISTERS  = 0xF0;
      public const byte PIO_STATUS_WRITE    = 0x55;
      public const byte PIO_STATUS_READ     = 0xAA;
      public const byte PIO_ACCESS_READ     = 0xF5;
      public const byte PIO_ACCESS_WRITE    = 0x5A;
      public const byte PIO_ACTIV_RESET     = 0xC3;
    }

    public class RegisterSelection  {
      public const byte I2C_STATUS          = 0xF0;
      public const byte I2C_READ_DATA       = 0xE1;
      public const byte CONFIGURATION       = 0xC3;
    }

    public class StatusBit {
      public const int BranchDirectionTaken   = 7;    // Branch Direction Taken (DIR)
      public const int TripletSecondBit       = 6;    // Triplet Second Bit (TSB)
      public const int SingleBitResult        = 5;    // Single Bit Result (SBR)
      public const int DeviceReset            = 4;    // Device Reset (RST)
      public const int LogicLevel             = 3;    // Logic Level (LL)
      public const int ShortDetected          = 2;    // Short Detected (SD)
      public const int PresencePulseDetected  = 1;    // Presence-Pulse Detect (PPD)
      public const int OneWireBusy            = 0;    // 1-Wire Busy (1WB)
    }

    public class TripletDirection {
      public const byte ONE   = 0x40;
      public const byte ZERO  = 0x00;
    }
  }
}
