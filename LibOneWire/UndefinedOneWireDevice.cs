// Copyright (c) Microsoft. All rights reserved.

namespace LibOneWire { 
  class UndefinedOneWireDevice : OneWireDevice {
    public DS2482_100 DS2482_100 { get; set; }
    public byte[] OneWireAddress { get; set; }
    public string Description;
    public byte   DeviceSts;

    public void Initialize(byte sts) {
      Description = "Dev Unknown";
      DeviceSts   = sts;

    } // End of Initialize
  }
}
