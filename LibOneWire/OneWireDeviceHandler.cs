// Copyright (c) Microsoft. All rights reserved.
using System;
using System.Collections.Generic;
using System.Linq;

/******************************************************************************************************************************************************
                              Maxim Integrated 1-Wire devices list

Family Code   Name            Description                     Notes 
 00	          Link locator	  Provide location information    illegal family code used to signal the link locator
 01	          DS2401
              DS2411
              DS1990R
              DS2490A         ID-only	 
 02	          DS1991	        Multikey	                      
 04	          DS2404	        EconoRam Time chi	              3-wire interface as well
 05	          DS2405	        Switch	                         
 06	          DS1993	        Memory	                        4Kb
 08	          DS1992	        Memory	                        1Kb
 09	          DS2502
              DS2703
              DS2704          Memory	                        
 0B	          DS2505	        Memory
 0F	          DS2506	        Memory
 10	          DS18S20	        Temperature
 12	          DS2406	        Switch	                        Also TAI8579baromter and T8A voltage
 14	          DS2430A	        Memory
 16	          DS1954
              DS1957          crypto-ibutton	                	 
 18	          DS1963S
              DS1962          SHA iButton
 1A	          DS1963L	        Monetary iButton
 1B	          DS2436	        Battery monitor
 1C	          DS28E04-100	    Switch
 1D	          DS2423	        Counter
 1E	          DS2437	        Battery monitor
 1F	          DS2409	        Microhub                        main and aux are subdirectories
 20	          DS2450	        Voltage
 21	          DS1921	        Thermocron
 22	          DS1922	        Temperature
 23	          DS2433
              DS1973          Memory
 24	          DS2415	        Clock
 26	          DS2438	        Battery monitor                 Also Humidiry sensors, Illuminescence, multisensors
 27	          DS2417	        Clock + interrupt
 28	          DS18B20	        Temperature                     Variable precision
 29	          DS2408	        Switch                          Also LCD controller
 2C	          DS2890	        Varible Resitor
 2D	          DS2431
              DS1972          Memory
 2E	          DS2770	        Battery
 30	          DS2760	        Battery
 31	          DS2720	        Battery ID
 32	          DS2780	        Battery
 33	          DS1961s
              DS2432          SHA-1 ibutton
 34	          DS2703	        SHA-1 Battery
 35	          DS2755	        Battery
 36	          DS2740	        Coulomb counter
 37	          DS1977	        password EEPROM
 3A	          DS2413	        Switch
 3B	          DS1825          Temperature
              MAX31826	      Temperature/memory
 3D	          DS2781	        Battery
 41	          DS1923	        Hygrocron
 42	          DS28EA00	      Temperature/IO
 43	          DS28EC20	      Memory
 44	          DS28E10	        SHA-1 Authenticator
 51	          DS2751	        Battery monitor
 7E	          EDS00xx	        Envoronmental Monitors        Temperature, Humidity, Barometric pressure, Light, Relay control, vibration, RTD thermisters 
 81	          USB id          ID found in DS2490R and DS2490B USB adapters        ID only but distinguished by the family code. 
 82	          DS1425	        Authorization	
 89	          DS1982U	        Uniqueware
 8B	          DS1985U	        Uniqueware
 8F	          DS1986U	        Uniqueware      
 A0	          mRS001	        Rotation Sensor               CMC Industrial Elactronits
 A1	          mVM001	        Vibratio                      CMC Industrial Elactronits
 A2	          mCM001	        AC Voltage	                  CMC Industrial Elactronics
 A6	          mTS017	        IR Temperature                CMC Industrial Elactronics
 B1	          mTC001	        Thermocouple Converter	      21 channels CMC Industrial Elactronics
 B2	          mAM001	        DC Current or Voltage         CMC Industrial Elactronics
 B3	          mTC002	        Thermocouple Converter	      168 channels CMC Industrial Elactronics
 EE	          UVI	 Ultra Violet Index                       Hobby Boards
 EF	          Moisture Hub    Moisture meter.4 Channel Hub  Hobby Boards
 FC	          BAE0910
              BAE0911         Programmable Miroprocessor	  Brain4Home entire system including fully programmable microprocessors
 FF	          LCD	            Swart LCD
  ****************************************************************************************************************************************************/
  

namespace LibOneWire {
  public sealed class OneWireDeviceHandler : IDisposable {
    DS2482_100 _ds2482_100;
    List<OneWireDevice> _oneWireDevices;
    Dictionary<byte, Type> _oneWireDeviceTypes;

    public IEnumerable<OneWireDevice> OneWireDevices {
      get {
        if (_oneWireDevices == null || !_oneWireDevices.Any()) {
          GetConnectedOneWireDevices();
        }
        return _oneWireDevices;
      }
    } // End of OneWireDevices

    // One wire device handler via DS2482-100
    // <param name="ad0">AD0 addess bit</param>
    // <param name="ad1">AD1 addess bit</param>
    // <exception cref="Agent.OneWire.DS2482100DeviceNotFoundException">Thrown if no DS2482-100 device is detected</exception>
    public OneWireDeviceHandler(bool ad0 = false, bool ad1 = false) {
      byte address = 0x18;            // 0011.000X

      if (ad0) {
        address |= 1 << 0;
      }
      if (ad1) {
        address |= 1 << 1;
      }
      try {
        _ds2482_100 = new DS2482_100(new I2cDeviceLocator().GetI2cDevice(address).Result);
        _ds2482_100.OneWireReset();
      }
      catch (Exception e) {
        throw new DS2482100DeviceNotFoundException(" No DS2482-100 detected", e);
      }
      _oneWireDeviceTypes = new Dictionary<byte, Type>();

      AddDeviceType<DS18B20>(0x10);

    } // End of OneWireDeviceHandler

    void GetConnectedOneWireDevices() {
      _oneWireDevices = new List<OneWireDevice>();
      var first = true;
      var deviceDetected = _ds2482_100.OneWireReset();

      if (deviceDetected) {
        var result = true;
        do {
          if (first) {
            first = false;
            result = _ds2482_100.OneWireFirst();
          }
          else {
            result = _ds2482_100.OneWireNext();
          }
          if (result) {
            AddOneWireDevice();
          }
        } while (result);
      }
    } // End of GetConnectedOneWireDevices

    void AddOneWireDevice() {
      if (_oneWireDeviceTypes.Any(k => k.Key == _ds2482_100.ROM_NO[0])) {
        var device            = (OneWireDevice)Activator.CreateInstance(_oneWireDeviceTypes.First(k => k.Key == _ds2482_100.ROM_NO[0]).Value);
        device.DS2482_100     = _ds2482_100;
        device.OneWireAddress = new byte[_ds2482_100.ROM_NO.Length];

        Array.Copy(_ds2482_100.ROM_NO, device.OneWireAddress, _ds2482_100.ROM_NO.Length);

        device.Initialize(_ds2482_100.DevStatus);
        _oneWireDevices.Add(device);
      }
      else {
        _oneWireDevices.Add(new UndefinedOneWireDevice { DS2482_100 = _ds2482_100, OneWireAddress = _ds2482_100.ROM_NO });
      }
    } // End of AddOneWireDevice

    public void AddDeviceType<T>(byte familyCode) where T : OneWireDevice {
      _oneWireDeviceTypes.Add(familyCode, typeof(T));
    } // End of AddDeviceType

    public IEnumerable<T> GetDevices<T>() where T : OneWireDevice {
      return OneWireDevices.GetDevices<T>();
    } // End of GetDevices

    public void Dispose() {
      if (_ds2482_100 != null)
        _ds2482_100.Dispose();
    } // End of Dispose
  }
}

