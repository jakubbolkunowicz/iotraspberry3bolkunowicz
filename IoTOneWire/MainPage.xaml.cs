/***************************************************************************************************************************************************************************
 * 
 *                                                                            ZMITAC IoT laboratory
 * 
 *        Raspberry Pi 1-Wire test application
 *        
 *        Hardware:
 *        1. Raspberry Pi /23 board
 *        2. DS2482-100 I2C to 1-Wire conversion board
 *        3. DS1820 1-Wire sensor chip
 *        4. LED connected to the Raspberry Pi GPIO-5 pin port
 *  
 *        System requirements:
 *        1. Windows 10 IoT Core OS system  Version 1809 (10.0; Build 17763)
 *        3. Windows 10 OS system OS        Version 1090 Build 18363.1082
 *        2. Visual Studio 2017             Version 15.9.27
 *        3. Universal Windows Platform     Version 10.0.17763.0
 * 
 *        Program description:
 *        The aim of this application is to demonstrate the ability to control 1-Wire devices from I2C Raspberry Pi board.
 *        1. Compiled for ARM platform the real Raspberry Pi and DS2482-100 conversion board are needed to work with DS1820 1-Wire temperature sensor
 *        2. Compiled for x86 platform doesn't require any hardware. The VS2017 UWP simulator is available to demonstrate basic application functionality
 *        
 ***************************************************************************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.System.Threading;
using LibOneWire;
using System.Diagnostics;
using Windows.UI.Core;
using Windows.Devices.Gpio;


namespace IoTOneWire {
    
    public sealed partial class MainPage : Page {
    ThreadPoolTimer PoolTimer, TimeTimer;
    private GpioPin gPinHeat;                       // GPIO control pins
        private SolidColorBrush greenBrush = new SolidColorBrush(Windows.UI.Colors.Green);
        private SolidColorBrush grayBrush = new SolidColorBrush(Windows.UI.Colors.Gray);
        private GpioPinValue pinValue1 = GpioPinValue.Low;
        private double settemp;
        private bool buf;
        //pomocnicze wartosci dla programu
        public MainPage() {
      this.InitializeComponent();

      Unloaded += MainPage_Unloaded;

      TimeTimer = ThreadPoolTimer.CreatePeriodicTimer(DateTimeDraw, TimeSpan.FromSeconds(1));
      PoolTimer = ThreadPoolTimer.CreatePeriodicTimer(OneWireChainLoop, TimeSpan.FromSeconds(2));

      

      InitGPIO();

            Dev_Heat.Checked += Dev_Heat_Checked;
            Dev_Heat.Unchecked += Dev_Heat_Unchecked;
            //Jesli przycisk zaznaczony lub odznaczony

        } // End of MainPage

    private void MainPage_Unloaded(object sender, object args) {
      // Cleanup
      gPinHeat.Dispose();

    } // End of MainPage_Unloaded

    private void InitGPIO() {
      var gpio = GpioController.GetDefault();

      //var tim = DateTime.Now.ToUniversalTime();
      //var CurTime = new TimeSpan(DateTime.Now.Ticks).TotalSeconds;
      var tim = DateTimeOffset.Now.ToUnixTimeMilliseconds().ToString();


      Time_Txt.Text = DateTime.Now.ToString("HH:mm:ss dd-MM-yyyy");

      // Show an error if there is no GPIO controller
      if (gpio == null) {
        gPinHeat = null;
        Dev_Sts.Text = "There is no GPIO controller on this device.";
        Debug.WriteLine("GPIO: No GPIO available!");
        return;
      }
      gPinHeat = gpio.OpenPin(5);     // Heat Pin
      gPinHeat.SetDriveMode(GpioPinDriveMode.Output);
      gPinHeat.Write(GpioPinValue.Low);

      Dev_Sts.Text = "GPIO: initialized.";
      Debug.WriteLine("GPIO: Initialized.");

    } // End of InitGPIO

    private async void DateTimeDraw(ThreadPoolTimer timer) {
      await Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () => { Time_Txt.Text = DateTime.Now.ToString("HH:mm:ss dd-MM-yyyy"); }).AsTask();
    } // End of DateTimeDraw



        private void Slider_SetTemperature(object sender, RangeBaseValueChangedEventArgs e) {
      if (Set_Temp != null)
      {
        Set_Temp.Text = string.Format("Set: {0:F1}°C", e.NewValue);
        settemp = e.NewValue;
        //dopisz temperature do bufora
      }
    } // End of Slider_SetTemperature

        //jesli zaznaczono Heater On
        private void Dev_Heat_Checked(object sender, RoutedEventArgs e)
        {
            buf = true;//zmien buf na true
        }

        //jesli odzaznaczono Heater On
        private void Dev_Heat_Unchecked(object sender, RoutedEventArgs e)
        {
            buf = false;//zmien buf na false
            pinValue1 = GpioPinValue.Low;
            Sts_Heat.Fill = pinValue1 == GpioPinValue.High ? greenBrush : grayBrush;
            //asynchroniczna aktualizacja LED
        }


        /****************************** I2C to 1-Wire sensors reading loop *********************************/
#if X86
        private async void OneWireChainLoop(ThreadPoolTimer timer) {
      DS18B20 device = new DS18B20();

      await Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () => { Dev_Sts.Text = "1-WIRE: reading sensor..."; }).AsTask();

            
            var Val = device.GetTemperature();

            await Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () => {
            Sts_Heat.Fill = pinValue1 == GpioPinValue.High ? greenBrush : grayBrush;
            //aktualizacja LED
            Debug.WriteLine("1-WIRE: {0} {1:F2}°C", device.OneWireAddressString(), Val);
            Dev_Temp.Text = string.Format("DS1820: {0} {1:F2}°C", device.OneWireAddressString(), Val);
                //jesli temperatura na termometrze wyzsza od zalozonej na sliderze
                //oraz jesli grzejnik jest podlaczony do pradu (wcisniety Heater On)
                if (Val < settemp && buf == true)
                {
                    //Uruchom grzejnik
                    pinValue1 = GpioPinValue.High;
                }
                else
                {
                    //Wylacz grzejnik
                    pinValue1 = GpioPinValue.Low;
                }

            Sts_Heat.Fill = pinValue1 == GpioPinValue.High ? greenBrush : grayBrush;
            //aktualizacja LED
            }).AsTask();

      await Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () => { Dev_Sts.Text = "1-WIRE: read sensor end."; }).AsTask();

    } // End of OneWireChainLoop
#else
    private async void OneWireChainLoop(ThreadPoolTimer timer){
      using (var oneWireDeviceHandler = new OneWireDeviceHandler()) {
        foreach (var device in oneWireDeviceHandler.GetDevices<DS18B20>()) {
          await Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () => { Dev_Sts.Text = "1-WIRE: reading sensor..."; }).AsTask();

          var Val = device.GetTemperatureS();
          if (device.DS2482_100.ReadOK == false)
            Debug.WriteLine(string.Format("1-WIRE: {0} CRC8 Error!", device.OneWireAddressStringS));
          else
            Debug.WriteLine("1-WIRE: {0} {1:F2}°C", device.OneWireAddressStringS, Val);

          await Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () => { Dev_Temp.Text = string.Format("DS1820: {0} {1:F2}°C", device.OneWireAddressStringS, Val); ; }).AsTask();
          await Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () => { Dev_Sts.Text = "1-WIRE: read sensor end."; }).AsTask();
        }
      }
    } // End of OneWireChainLoop
#endif
  }
}

